import React, { PropTypes } from 'react';
import { Provider } from 'react-redux';
import { Route, Router, IndexRoute } from 'react-router';

// Config
import { SIGN_IN_PATH, TASKS_PATH, PRODUCT_PATH, ADD_PRODUCT_PATH, EDIT_PRODUCT_PATH } from 'src/config';

// Components
import App from './app/app';
import SignIn from './sign-in/sign-in';
import Product from './product/product';
import AddProduct from './product/addProduct';
import EditProduct from './product/EditProduct';
import Tasks from './tasks/tasks';


export function Root({history, onEnter, store}) {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Route component={App} onEnter={onEnter} path="/">
          <IndexRoute component={SignIn} />
          <Route component={SignIn} path={SIGN_IN_PATH} />
          <Route component={Product} path={PRODUCT_PATH} />
          <Route component={AddProduct} path={ADD_PRODUCT_PATH} />
          <Route component={EditProduct} path={EDIT_PRODUCT_PATH} />
          <Route component={Tasks} path={TASKS_PATH} />
        </Route>
      </Router>
    </Provider>
  );
}

Root.propTypes = {
  history: PropTypes.object.isRequired,
  onEnter: PropTypes.func.isRequired,
  store: PropTypes.object.isRequired
};
