export const FIREBASE_CONFIG = {
  apiKey: 'AIzaSyBDJBP5zdMYV2bnadFaun9Kijs23lYYwmo',
  authDomain: 'fireproduct.firebaseapp.com',
  databaseURL: 'https://fireproduct.firebaseio.com',
  storageBucket: 'project-2508583925533246495.appspot.com'
};


// Route paths
export const SIGN_IN_PATH = '/sign-in';
export const PRODUCT_PATH = '/products';
export const ADD_PRODUCT_PATH = '/product/add';
export const EDIT_PRODUCT_PATH = '/product/:product_id/edit';
export const TASKS_PATH = '/tasks';
export const POST_SIGN_IN_PATH = PRODUCT_PATH;
export const POST_SIGN_OUT_PATH = SIGN_IN_PATH;
