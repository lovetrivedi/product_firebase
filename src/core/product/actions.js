import { firebaseDb, firesbaseStorage } from 'src/core/firebase';
import {
  CREATE_PRODUCT_ERROR,
  CREATE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_ERROR,
  DELETE_PRODUCT_SUCCESS,
  UPDATE_PRODUCT_ERROR,
  UPDATE_PRODUCT_SUCCESS,
  PRODUCT_LISTENERS_REGISTERED,
  PRODUCT_LOADING_START,
  PRODUCT_LOADING_END
} from './action-types';

var storageRef = firesbaseStorage.ref();

export function getProductDetail() {
  return 'test';
}

export function createProduct(product, file) {
  return (dispatch, getState) => {
    const { auth } = getState();
    dispatch({type: PRODUCT_LOADING_START});
    var imageName = product.name + '_' + product.sku + '_' + product.supplier + '_' + file.name;
    var uploadTask = storageRef.child('/images/' + imageName).put(file);
    uploadTask.on('state_changed', function() {
    }, function(error) {
      console.error('ERROR @ createTask :', error); // eslint-disable-line no-console
    }, function() {
      var downloadURL = uploadTask.snapshot.downloadURL;
      product.image = downloadURL;
      product.image_name = imageName;
      firebaseDb.ref(`products/${auth.id}`)
      .push(product, error => {
        if (error) {
          console.error('ERROR @ createTask :', error); // eslint-disable-line no-console
          dispatch({
            type: CREATE_PRODUCT_ERROR,
            payload: error
          });
        }
      });
    });
  };
}

export function deleteProduct(product) {
  return (dispatch, getState) => {
    const { auth } = getState();
    dispatch({type: PRODUCT_LOADING_START});
    var deleteTask = storageRef.child('images/' + product.image_name);
    deleteTask.delete().then(function() {
      firebaseDb.ref(`products/${auth.id}/${product.key}`)
      .remove(error => {
        if (error) {
          console.error('ERROR @ deleteTask :', error); // eslint-disable-line no-console
          dispatch({
            type: DELETE_PRODUCT_ERROR,
            payload: error
          });
        }
      });
    }).catch(function(error) {
      console.log(error); // eslint-disable-line no-console
    });
  };
}


export function undeleteProduct() {
  return (dispatch, getState) => {
    const { auth, products } = getState();
    const task = products.deleted;
    dispatch({type: PRODUCT_LOADING_START});
    firebaseDb.ref(`products/${auth.id}/${task.key}`)
      .set({completed: task.completed, title: task.title}, error => {
        if (error) {
          console.error('ERROR @ undeleteTask :', error); // eslint-disable-line no-console
        }
      });
  };
}


export function updateProduct(product, file) {
  return (dispatch, getState) => {
    const { auth } = getState();
    dispatch({type: PRODUCT_LOADING_START});
    if (file) {
      var uploadTask = storageRef.child('/images/' + product.image_name).put(file);
      uploadTask.on('state_changed', function() {
      }, function(error) {
        console.error('ERROR @ update product :', error); // eslint-disable-line no-console
      }, function() {
        firebaseDb.ref(`products/${auth.id}/${product.key}`)
          .update(product, error => {
            if (error) {
              console.error('ERROR @ update product :', error); // eslint-disable-line no-console
              dispatch({
                type: UPDATE_PRODUCT_ERROR,
                payload: error
              });
            }
          });
      });
    }
    else {
      firebaseDb.ref(`products/${auth.id}/${product.key}`)
        .update(product, error => {
          if (error) {
            console.error('ERROR @ updateTask :', error); // eslint-disable-line no-console
            dispatch({
              type: UPDATE_PRODUCT_ERROR,
              payload: error
            });
          }
        });
    }
  };
}


export function registerListeners() {
  return (dispatch, getState) => {
    const { auth } = getState();

    dispatch({type: PRODUCT_LOADING_START});

    const ref = firebaseDb.ref(`products/${auth.id}`);

    ref.on('child_added', snapshot => dispatch({
      type: CREATE_PRODUCT_SUCCESS,
      payload: recordFromSnapshot(snapshot)
    }));

    ref.on('child_changed', snapshot => dispatch({
      type: UPDATE_PRODUCT_SUCCESS,
      payload: recordFromSnapshot(snapshot)
    }));

    ref.on('child_removed', snapshot => dispatch({
      type: DELETE_PRODUCT_SUCCESS,
      payload: recordFromSnapshot(snapshot)
    }));

    dispatch({type: PRODUCT_LISTENERS_REGISTERED});
  };
}


function recordFromSnapshot(snapshot) {
  let record = snapshot.val();
  record.key = snapshot.key;
  return record;
}
